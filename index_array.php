<?php

require_once __DIR__ . '/vendor/autoload.php';

use Mpdf\Config\FontVariables;
use Mpdf\Config\ConfigVariables;
use Mpdf\Mpdf;

//require 'vendor/autoload.php';

$defaultConfig = (new ConfigVariables())->getDefaults();
$fontDirs = array_merge($defaultConfig['fontDir'], [
    __DIR__ . '/assets/fonts/'
]);

$defaultFontConfig = (new FontVariables())->getDefaults();
$fontData = array_merge($defaultFontConfig['fontdata'], [
    'opensans' => [
        'R' => 'ProximaNova-Regular.ttf',
        'B' => 'ProximaNova-Bold.ttf',
    ]
]);

// $html = file_get_contents('assets/template/index.html');
//$html = file_get_contents('assets/template/index-cup-of-ukraine-gorizont.html');
//$html = file_get_contents('assets/template/cup-of-ukraine-2.html');
$html = file_get_contents('assets/template/codes.html');

$files = scandir(dirname(__FILE__) . '/assets/barcode/');

foreach ($files as $code) {
    if ( strpos($code, '.gif') !== false ) {
        $html .= '<div class="code"><div><img width="411" height="200" src="assets/barcode/'. $code .'" alt=""></div></div>';
    }
}

$html .= '</body></html>';
//$html = file_get_contents('assets/template/electronic-concert-ticket-upark.html');
//$html = file_get_contents('assets/template/index_tov.html');
//$html = file_get_contents('assets/template/index_unl.html');
// $html = file_get_contents('assets/template/electronic-football-ticket-shahtar-ingulec.html');
// for mini ticket
//$dpi = 180;
//$format = [152, 75];
$dpi = 180;
$format = [58, 40];
// for A4

// $dpi = 150;
// $format = [210, 297];

try {
    $pdf = new \Mpdf\Mpdf([
        'mode' => 'BLANK',
        'format' => $format,
        'dpi' => $dpi,
        'img_dpi' => $dpi,
        'margin_top' => 0,
        'margin_bottom' => 0,
        'margin_left' => 0,
        'margin_right' => 0,
        'fontDir' => $fontDirs,
        'fontdata' => $fontData,
        'default_font' => 'proxima',
        'allow_output_buffering' => true,
    ]);
    $pdf->WriteHTML($html);

    $pdf->Output();
} catch (\Mpdf\MpdfException $e) {
    echo 'fuck';
}